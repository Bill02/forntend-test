import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        articlePageContent: {}
    },
    // getters: {
    //     itemList (state) {
    //         return state.items;
    //     },
    // },
    // actions: {
    //     addItem ({ commit }, item) {
    //         commit('pushItem', item);
    //     },
    // },
    mutations: {
        pushItem (state, article) {
            state.articlePageContent = article;
        }
    }

})

export default store